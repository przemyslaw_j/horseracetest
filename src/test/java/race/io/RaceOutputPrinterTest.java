package race.io;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import race.HorseRaceResult;

import com.google.common.collect.Lists;

/**
 * Unit tests for RaceOutputPrinter.
 */
@RunWith(JUnit4.class)
public class RaceOutputPrinterTest {

  private RaceOutputPrinter raceOutputPrinter;
  private final ByteArrayOutputStream outputBytes = new ByteArrayOutputStream();
  private final String outputEncoding = "UTF8";

  @Before
  public void setUp() {
    raceOutputPrinter = new RaceOutputPrinter(new PrintStream(outputBytes));
  }

  private String outputToString() throws UnsupportedEncodingException {
    return outputBytes.toString(outputEncoding);
  }

  @Test
  public void print_NoPlayers() throws UnsupportedEncodingException {
    HorseRaceResult testResult = new HorseRaceResult(
        new ArrayList<String>(), new ArrayList<Integer>());
    raceOutputPrinter.print(testResult);
    assertEquals(RaceOutputPrinter.HEADER + "\n", outputToString());
  }

  @Test
  public void print_ManyPlayers() throws UnsupportedEncodingException {
    HorseRaceResult testResults = new HorseRaceResult(
        Lists.newArrayList("Player1", "Player2", "Player3"),
        Lists.newArrayList(3, 1, 2));
    raceOutputPrinter.print(testResults);
    String expectedResults = "" 
        + RaceOutputPrinter.HEADER
        + "\n"
        + "1, 2, Player2\n"
        + "2, 3, Player3\n"
        + "3, 1, Player1\n";
    assertEquals(expectedResults, outputToString());
  }
}
