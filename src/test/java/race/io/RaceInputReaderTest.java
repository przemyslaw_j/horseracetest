package race.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.StringReader;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import race.BallThrow;

import com.google.common.collect.Lists;

/**
 * Unit tests for RaceInputReader.
 */
@RunWith(JUnit4.class)
public class RaceInputReaderTest {

  private RaceInputReader raceInputReader;

  private void setUpReader(String input) {
    raceInputReader = new RaceInputReader(
        new BufferedReader(new StringReader(input)));
  }

  @Test
  public void readPlayerNames() {
    setUpReader("Player1, Player2, Player3");
    assertEquals(
        Lists.newArrayList("Player1", "Player2", "Player3"),
        raceInputReader.readPlayerNames());
  }

  @Test
  public void getBallTrowsIterator_ReadBeforePlayers() {
    setUpReader("Player1, Player2, Player3");
    try {
      raceInputReader.getBallThrowsIterator();
      fail();
    } catch (IllegalStateException e) {
      // expected
    }
  }

  @Test
  public void getBallTrowsIterator_IncorrectFormat() {
    String[] testedFormats = new String[] {
        "Player1, Player2, Player3\nPlayer1 10\n",
        "Player1, Player2, Player3\n1 10 1\n",
        "Player1, Player2, Player3\n8 10\n" };
    for (String incorrectFormat : testedFormats) {
      setUpReader(incorrectFormat);
      try {
        raceInputReader.getBallThrowsIterator();
        fail("Should fail when reading description of a throw:"
            + incorrectFormat);
      } catch (IllegalStateException e) {
        // expected
      }
    }
  }

  @Test
  public void getBallTrowsIterator_CorrectFormat() {
    setUpReader("" + "Player1, Player2, Player3\n" + "2 10\n" + "3 20\n"
        + "1 50\n");
    raceInputReader.readPlayerNames();
    assertEquals(
        Lists.newArrayList(new BallThrow(2, 10), new BallThrow(3, 20),
            new BallThrow(1, 50)),
        Lists.newArrayList(raceInputReader.getBallThrowsIterator()));
  }
}
