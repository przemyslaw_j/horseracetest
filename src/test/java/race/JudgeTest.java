package race;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import com.google.common.collect.Lists;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * Unit tests for Judge.
 */
@RunWith(JUnit4.class)
public class JudgeTest {

  private Judge judge = new Judge();

  @Test
  public void getHorseRaceRanking_FinishedRace() {
    judge.addMove(new BallThrow(1, 20));
    judge.addMove(new BallThrow(2, 150));
    judge.addMove(new BallThrow(3, 100));
    judge.addMove(new BallThrow(3, 100));
    judge.addMove(new BallThrow(1, 200));
    assertTrue(judge.isRaceFinished());
    assertEquals(Lists.newArrayList(1, 3, 2), judge.getHorceRaceRanking());
  }

  @Test
  public void getHorseRaceRanking_UnfinishedRace() {
    for (int i = 0; i < (Judge.RACE_DISTANCE_YARDS / 10) - 1; i += 1) {
      judge.addMove(new BallThrow(2, 10));
      assertFalse(judge.isRaceFinished());
    }
  }

}
