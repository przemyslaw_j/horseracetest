package race;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import race.io.RaceInputReader;

import com.google.common.collect.Lists;

/**
 * Unit tests for HorseRace.
 */
@RunWith(JUnit4.class)
public class HorseRaceTest {

  @Mock
  RaceInputReader raceInputReader;
  @Mock
  Judge judge;

  private final List<String> expectedPlayerNames = Lists.newArrayList(
      "Player1", "Player2");

  private HorseRace horseRace;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
    horseRace = new HorseRace(raceInputReader, judge);
    when(raceInputReader.readPlayerNames()).thenReturn(expectedPlayerNames);
  }

  @Test
  public void run_NoWinner() {
    when(raceInputReader.getBallThrowsIterator()).thenReturn(
        Lists.<BallThrow> newArrayList().iterator());
    try {
      horseRace.run();
      fail();
    } catch (IllegalStateException e) {
      // expected
    }
  }

  @Test
  public void run_WithWinner() {
    BallThrow testThrow = new BallThrow(1, 2);
    when(raceInputReader.getBallThrowsIterator()).thenReturn(
        Lists.newArrayList(testThrow).iterator());
    List<Integer> expectedPlayersRanking = Lists.newArrayList(2, 1);
    when(judge.isRaceFinished()).thenReturn(false, true);
    when(judge.getHorceRaceRanking()).thenReturn(expectedPlayersRanking);
    HorseRaceResult result = horseRace.run();
    assertEquals(expectedPlayerNames, result.getPlayerNames());
    assertEquals(expectedPlayersRanking, result.getPlayersRank());
    verify(judge).addMove(testThrow);
  }
}
