package race;

/**
 * Represents single throw of a ball during the race.
 */
public class BallThrow {

  private final int playerLane;
  private final int distance;

  public BallThrow(int playerLane, int distance) {
    this.playerLane = playerLane;
    this.distance = distance;
  }

  public int getPlayerLane() {
    return playerLane;
  }

  public int getDistance() {
    return distance;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null || !(obj instanceof BallThrow))
      return false;
    BallThrow other = (BallThrow) obj;
    return (distance == other.distance) && (playerLane == other.playerLane);
  }

  @Override
  public String toString() {
    return String.format("PlayerLane: %d, Distance: %d", playerLane, distance);
  }
}
