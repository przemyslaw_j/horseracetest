package race.io;

import java.io.PrintStream;

import race.HorseRaceResult;

/**
 * Prints to the given {@code PrintStream} results of the race.
 */
public class RaceOutputPrinter {

  protected static final String HEADER = "Position, Lane, Horse name";

  private final PrintStream output;

  public RaceOutputPrinter(PrintStream output) {
    this.output = output;
  }

  public void print(HorseRaceResult results) {
    output.println(HEADER);
    int playersNo = results.getPlayerNames().size();
    String[] rankingLines = new String[playersNo];
    int lane = 1;
    for (int rank : results.getPlayersRank()) {
      rankingLines[rank - 1] = String.format("%d, %d, %s", rank, lane, results
          .getPlayerNames().get(lane - 1));
      ++lane;
    }
    for (String line : rankingLines) {
      output.println(line);
    }
  }

}
