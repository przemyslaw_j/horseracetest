package race.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import race.BallThrow;

import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.AbstractIterator;
import com.google.common.collect.Lists;

/**
 * Reads the description of the race from the input buffer.
 * 
 * The class assumes following format of the input:
 * <ul>
 * <li>Player names separated with commas in the first line.
 * <li>Each following line has format: 'player_lane distance'.
 * </ul>
 */
public class RaceInputReader {

  private final static int MAX_NUMBER_OF_LANES = 7;

  private final BufferedReader bufferedReader;

  private boolean readPlayersLine;

  public RaceInputReader(BufferedReader bufferedReader) {
    this.bufferedReader = bufferedReader;
  }

  public List<String> readPlayerNames() {
    Preconditions.checkState(!readPlayersLine);
    readPlayersLine = true;
    try {
      String line = bufferedReader.readLine();
      return Lists.newArrayList(Splitter.on(", ").split(line));
    } catch (IOException e) {
      throw new IllegalArgumentException("Can not read the input");
    }
  }

  /**
   * Returns BallThrows in the order read from the input stream. Should be
   * called after reading the player names.
   * 
   * @throw IllegalArgumentException if the format of the input is incorrect.
   */
  public Iterator<BallThrow> getBallThrowsIterator() {
    Preconditions.checkState(readPlayersLine);

    return new AbstractIterator<BallThrow>() {

      @Override
      protected BallThrow computeNext() {
        String line = null;
        try {
          line = bufferedReader.readLine();
        } catch (IOException e) {
          throw new IllegalArgumentException("can not read the input");
        }

        if (line == null) {
          return this.endOfData();
        }

        String[] parts = line.split(" ");
        Preconditions.checkState(parts.length == 2,
            "Incorrect input format in line: " + line);
        int playerLane = Integer.parseInt(parts[0]);
        Preconditions.checkState(playerLane > 0
            && playerLane <= MAX_NUMBER_OF_LANES,
            "out of bounds lane of the player in the input line:" + line);
        return new BallThrow(playerLane, Integer.parseInt(parts[1]));
      }
    };
  }
}
