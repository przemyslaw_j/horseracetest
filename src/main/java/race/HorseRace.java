package race;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;

import race.io.RaceInputReader;
import race.io.RaceOutputPrinter;

import com.google.common.base.Charsets;

/**
 * The program simulates horse racing game as per details described in:
 * https://github.com/intenthq/horse-racing.
 * 
 * <p>
 * It takes as an input file with the description of the race. The first line
 * should list the players participating in the game: 
 * 'PlayerName1,..,PlayerNainputFileN' 
 * Each following line describes a throw by a single
 * player 'player_lane distance' where both player_lane and distance are
 * numbers.
 */
public class HorseRace {

  private final RaceInputReader raceInputReader;
  private final Judge judge;

  public HorseRace(RaceInputReader raceInputReader, Judge judge) {
    this.raceInputReader = raceInputReader;
    this.judge = judge;
  }

  public static void main(String[] args) {
    if (args.length < 1) {
      System.err.println(
          "Pass file name with the program input as the argument");
      System.exit(1);
    }
    String inputFile = args[0];
    BufferedReader bufferedReader = createInputReader(inputFile);
    HorseRace horseRace = new HorseRace(
        new RaceInputReader(bufferedReader), new Judge());
    HorseRaceResult results = horseRace.run();
    RaceOutputPrinter outputPrinter = new RaceOutputPrinter(System.out);
    outputPrinter.print(results);
    try {
      bufferedReader.close();
    } catch (IOException e) {
      System.err.printf(
          "Failed to close file %s after reding the input", inputFile);
    }
  }

  /**
   * Returns results of the horse race game.
   */
  public HorseRaceResult run() {
    List<String> playerNames = raceInputReader.readPlayerNames();
    Iterator<BallThrow> ballThrows = raceInputReader.getBallThrowsIterator();
    while (ballThrows.hasNext() && !judge.isRaceFinished()) {
      judge.addMove(ballThrows.next());
    }
    if (!judge.isRaceFinished()) {
      throw new IllegalStateException("Input file too short, no winners found");
    }
    return new HorseRaceResult(playerNames, judge.getHorceRaceRanking());
  }

  /**
   * Returns new {@link BufferReader} reading from the input file.
   */
  private static BufferedReader createInputReader(String inputFile) {
    FileInputStream fileStream = null;
    try {
      fileStream = new FileInputStream(inputFile);
    } catch (FileNotFoundException e) {
      System.err.printf("Input file %s not found", inputFile);
      System.exit(1);
    }
    return new BufferedReader(new InputStreamReader(fileStream, Charsets.UTF_8));
  }

}
