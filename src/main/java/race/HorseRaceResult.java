package race;

import java.util.List;

/**
 * Holds data with Horse Race results.
 */
public class HorseRaceResult {

  private final List<String> playerNames;
  private final Iterable<Integer> playersRank;

  public HorseRaceResult(List<String> playerNames, Iterable<Integer> playersRank) {
    this.playerNames = playerNames;
    this.playersRank = playersRank;
  }

  public List<String> getPlayerNames() {
    return playerNames;
  }

  public Iterable<Integer> getPlayersRank() {
    return playersRank;
  }
}
