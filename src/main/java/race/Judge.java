package race;

import java.util.HashMap;
import java.util.Map;

import com.google.common.base.Functions;
import com.google.common.base.Optional;
import com.google.common.collect.Ordering;

/**
 * Computes race results for the given {@code BallThrow}.
 */
public class Judge {

  protected static final int RACE_DISTANCE_YARDS = 220;

  private final Map<Integer, Integer> distanceCovered = new HashMap<Integer, Integer>();
  private boolean isFinished;

  public boolean isRaceFinished() {
    return isFinished;
  }

  public void addMove(BallThrow ballThrow) {
    int distanceBefore = Optional.fromNullable(
        distanceCovered.get(ballThrow.getPlayerLane())).or(0);
    int newDistance = distanceBefore + ballThrow.getDistance();
    distanceCovered.put(ballThrow.getPlayerLane(), newDistance);
    if (!isFinished && newDistance >= RACE_DISTANCE_YARDS) {
      isFinished = true;
    }
  }

  /**
   * Returns positions of the players in the final ranking. It assumes the
   * players took adjacent lanes starting from lane no 1., and returns ranks in
   * that order for the lanes. Draws are settled by ordering the drawing
   * players randomly.
   */
  public Iterable<Integer> getHorceRaceRanking() {
    return Ordering.natural().reverse()
        .onResultOf(Functions.forMap(distanceCovered))
        .immutableSortedCopy(distanceCovered.keySet());
  }

}
