README
==========
The program runs horse game simulation as per specification. As its argument it expects an input file with the game description.

The code is split into two packages:

1. `race`: which contains logic 
2. `race.io` which handles reading and writing to and from the output

`HorseRace.java` is the main class which could be good start for the code review.

#Assumptions made
- Input file is encoded using utf-8 - important for the players' names
- The players take subsequent names, i.e. if there is four players they use lanes 1-4

#Things worth improving
- validation of the input file and error messages
- run of the verification tests as one of the gradle tasks

#Run instructions

##Requisites
- Java
- Gradle

##Run
- Build and run unit tests
`$ ./gradlew test`
- Create jar with its dependencies
`$./gradlew shadowJar`
- To run for an input_file:
`java -jar build/libs/horseracetest-1.0-all.jar input_file`

To verify the program against the test input run:

`$ java -jar build/libs/horseracetest-1.0-all.jar src/test/resources/input.txt > build/tmp/output.txt ; diff src/test/resources/output.txt build/tmp/output.txt`
